package queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BQueue {

	private Stash mStash;

	private final int STASH_LIMIT = 1001;

	public BQueue() {
		// TODO Auto-generated constructor stub
		mStash = new Stash(STASH_LIMIT);
		new Producer();
		new Consumer();
	}

	/**
	 * @param args
	 */
	public static void main(String[] atgs) {
		// TODO Auto-generated method stub
		new BQueue();
	}

	private class Producer implements Runnable {

		private final long PRODUCER_PRODUCE_ELEMENT = 400;
		private Thread mThread;

		public Producer() {
			// TODO Auto-generated constructor stub
			mThread = new Thread(this, getClass().getName());
			mThread.start();
		}

		public void run() {
			// TODO Auto-generated method stub
			while (true) {
				try {
					mStash.createProduct();
					Thread.sleep(PRODUCER_PRODUCE_ELEMENT);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private class Consumer implements Runnable {

		private final long CONSUMER_SELL_PRODUCT = 500;
		private Thread mThread;

		public Consumer() {
			// TODO Auto-generated constructor stub
			mThread = new Thread(this, getClass().getName());
			mThread.start();
		}

		public void run() {
			// TODO Auto-generated method stub
			while (true) {
				try {
					mStash.sellProduct();
					Thread.sleep(CONSUMER_SELL_PRODUCT);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	private class Stash {

		private BlockingQueue<Integer> mQueue;
		private int mStashLimit = 0;
		private int mProduct = 0;

		public Stash(int stashLimit) {
			// TODO Auto-generated constructor stub
			mStashLimit = stashLimit;
			mQueue = new ArrayBlockingQueue<Integer>(mStashLimit);
		}

		public synchronized void createProduct() throws InterruptedException {
			mProduct++;
			mQueue.put(mProduct);

			System.out.println(Thread.currentThread().getName() + ". Create: "
					+ mQueue.size());
		}

		public synchronized void sellProduct() throws InterruptedException {
			mProduct--;
			mQueue.take();

			System.out.println(Thread.currentThread().getName() + ". Sell: "
					+ mQueue.size());
		}
	}

	
}
