package main;

public class RunTask {

	Stash stash;

	public RunTask() {
		// TODO Auto-generated constructor stub

		stash = new Stash();
		new Consumer();
		new Producer();

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new RunTask();
	}

	private class Producer implements Runnable {

		private Thread mThread;

		public Producer() {
			// TODO Auto-generated constructor stub
			mThread = new Thread(this, "Producer");
			mThread.start();
		}

		public void run() {
			// TODO Auto-generated method stub
			try {
				while (true) {
					stash.createNewProduct();
					Thread.sleep(100);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private class Consumer implements Runnable {

		private Thread mThread;

		public Consumer() {
			// TODO Auto-generated constructor stub
			mThread = new Thread(this, "Consumer");
			mThread.start();
		}

		public void run() {
			// TODO Auto-generated method stub
			try {
				while (true) {
					stash.sellProduct();
					Thread.sleep(500);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private class Stash {
		
		private int mProduct = 0;
		private int mStashSize = 10;

		public Stash() {
			// TODO Auto-generated constructor stub
		}

		public synchronized void createNewProduct() throws InterruptedException {
			while (mProduct >= mStashSize) {
				wait();
			}

			mProduct++;
			System.out.println("New product. Size: " + mProduct);
			notifyAll();
		}

		public synchronized void sellProduct() throws InterruptedException {
			while (mProduct == 0) {
				wait();
			}

			mProduct--;
			System.out.println("Sell product. Size: " + mProduct);
			notifyAll();
		}
	}
	
}
